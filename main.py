#Ввод координат векторов
import timeit
import matplotlib.pyplot as plt


x = []
y = []
z = []
print('Введите координаты вектора x')
for i in range(3):
    x_var = int(input())
    x.append(x_var)
print('Введите координаты вектора y')
for i in range(3):
    y_var = int(input())
    y.append(y_var)
a = float(input('Введите скаляр a'))
starttime = timeit.default_timer()
print("начальное время:", starttime)
zx = x[0] * y[0] * a
zy = x[1] * y[1] * a
zz = x[2] * y[2] * a
z.append(zx)
z.append(zy)
z.append(zz)
print('Вектор z - ', z)
print("затраченное время:", timeit.default_timer() - starttime)


plt.plot([8, 98994, 197989, 296984], [5.710000000025417e-05, 5.3999999998666226e-05, 7.719999999977745e-05, 4.950000000025767e-05],
         linewidth=2.0, label="целочисленные")
plt.plot([15, 98997, 197993, 296989], [7.239999999875124e-05, 0.00024360000000100968, 5.9000000000253294e-05, 5.930000000020641e-05],
         linewidth=1.0, label="вещественные")
plt.xlabel('длина вектора')
plt.ylabel('время')
plt.legend()
plt.show()

